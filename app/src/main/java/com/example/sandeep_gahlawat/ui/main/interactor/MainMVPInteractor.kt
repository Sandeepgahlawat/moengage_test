package com.example.sandeep_gahlawat.ui.main.interactor

import com.example.sandeep_gahlawat.data.network.Article
import com.example.sandeep_gahlawat.data.network.ArticleResponse
import com.example.sandeep_gahlawat.ui.base.interactor.MVPInteractor
import io.reactivex.Observable


interface MainMVPInteractor : MVPInteractor {
    fun getDataFromBlog(): Observable<ArticleResponse>

    fun insertIntoDb(articles:List<Article>) : Observable<Boolean>

    fun checkIfArticlesDbEmpty():Observable<Boolean>

    fun getAllArticlesFromDb(): Observable<List<Article>>
}