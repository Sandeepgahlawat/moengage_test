package com.example.sandeep_gahlawat.ui.main.view

import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sandeep_gahlawat.R
import com.example.sandeep_gahlawat.data.network.Article
import com.example.sandeep_gahlawat.ui.base.views.BaseActivity
import com.example.sandeep_gahlawat.ui.main.interactor.MainMVPInteractor
import com.example.sandeep_gahlawat.ui.main.presenter.MainMVPPresenter
import javax.inject.Inject


class MainActivity : BaseActivity(), MainMVPView {

    @Inject
    internal lateinit var presenter: MainMVPPresenter<MainMVPView, MainMVPInteractor>

    @Inject
    internal lateinit var articleAdapter: ArticleAdapter

    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    lateinit var articleRecyclerView: RecyclerView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onAttach(this)
        setUp()
    }

    fun setUp() {
        articleRecyclerView = findViewById(R.id.articleRecyclerView)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        articleRecyclerView.layoutManager = layoutManager
        articleRecyclerView.itemAnimator = DefaultItemAnimator()
        articleRecyclerView.adapter = articleAdapter
        presenter.getDataFromBlog()
    }



    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun displayArticles(articles: List<Article>)= articles.let {
        articleAdapter.addArticlesToList(articles)
    }

}
