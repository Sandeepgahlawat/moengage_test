package com.example.sandeep_gahlawat.ui.main.interactor

import com.example.sandeep_gahlawat.data.database.repository.ArticlesRepo
import com.example.sandeep_gahlawat.data.network.ApiHelper
import com.example.sandeep_gahlawat.data.network.Article
import com.example.sandeep_gahlawat.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject


class MainInteractor @Inject internal constructor(
    apiHelper: ApiHelper,
    private val articlesRepo: ArticlesRepo
) : BaseInteractor(apiHelper = apiHelper), MainMVPInteractor {
    override fun insertIntoDb(articles: List<Article>): Observable<Boolean> = articlesRepo.insertArticle(articles)

    override fun checkIfArticlesDbEmpty(): Observable<Boolean> = articlesRepo.isArticlesRepoEmpty()

    override fun getDataFromBlog() = apiHelper.getBlogApiCall()

    override fun getAllArticlesFromDb(): Observable<List<Article>> = articlesRepo.loadArticles()
}


