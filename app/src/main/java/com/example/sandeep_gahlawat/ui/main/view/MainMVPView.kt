package com.example.sandeep_gahlawat.ui.main.view

import com.example.sandeep_gahlawat.data.network.Article
import com.example.sandeep_gahlawat.ui.base.views.MVPView


interface MainMVPView : MVPView {


    fun displayArticles(articles:List<Article>)
}