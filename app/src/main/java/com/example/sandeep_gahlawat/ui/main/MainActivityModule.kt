package com.example.sandeep_gahlawat.ui.main

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sandeep_gahlawat.ui.main.interactor.MainMVPInteractor
import com.example.sandeep_gahlawat.ui.main.presenter.MainMVPPresenter
import com.example.sandeep_gahlawat.ui.main.presenter.MainPresenter
import com.example.sandeep_gahlawat.ui.main.view.MainMVPView
import com.example.sandeep_gahlawat.ui.main.interactor.MainInteractor
import com.example.sandeep_gahlawat.ui.main.view.ArticleAdapter
import com.example.sandeep_gahlawat.ui.main.view.MainActivity
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    internal fun provideMainInteractor(mainInteractor: MainInteractor): MainMVPInteractor = mainInteractor

    @Provides
    internal fun provideMainPresenter(mainPresenter: MainPresenter<MainMVPView, MainMVPInteractor>)
            : MainMVPPresenter<MainMVPView, MainMVPInteractor> = mainPresenter

    @Provides
    internal fun provideBlogAdapter(): ArticleAdapter = ArticleAdapter(ArrayList())

    @Provides
    internal fun provideLinearLayoutManager(activity: MainActivity): LinearLayoutManager = LinearLayoutManager(activity)

}