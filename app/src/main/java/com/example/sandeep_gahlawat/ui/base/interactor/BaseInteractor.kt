package com.example.sandeep_gahlawat.ui.base.interactor

import com.example.sandeep_gahlawat.data.network.ApiHelper

open class BaseInteractor() : MVPInteractor {

    protected lateinit var apiHelper: ApiHelper

    constructor(apiHelper: ApiHelper) : this() {
        this.apiHelper = apiHelper
    }


}