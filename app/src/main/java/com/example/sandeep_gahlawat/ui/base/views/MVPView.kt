package com.example.sandeep_gahlawat.ui.base.views

interface MVPView {

    fun showProgress()

    fun hideProgress()

}