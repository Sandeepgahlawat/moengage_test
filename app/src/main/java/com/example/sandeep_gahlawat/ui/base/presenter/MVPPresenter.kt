package com.example.sandeep_gahlawat.ui.base.presenter

import com.example.sandeep_gahlawat.ui.base.interactor.MVPInteractor
import com.example.sandeep_gahlawat.ui.base.views.MVPView

interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}