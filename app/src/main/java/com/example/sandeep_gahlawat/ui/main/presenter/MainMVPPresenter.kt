package com.example.sandeep_gahlawat.ui.main.presenter

import com.example.sandeep_gahlawat.ui.base.presenter.MVPPresenter
import com.example.sandeep_gahlawat.ui.main.interactor.MainMVPInteractor
import com.example.sandeep_gahlawat.ui.main.view.MainMVPView


interface MainMVPPresenter<V : MainMVPView, I : MainMVPInteractor> : MVPPresenter<V, I> {
    fun getDataFromBlog()
}