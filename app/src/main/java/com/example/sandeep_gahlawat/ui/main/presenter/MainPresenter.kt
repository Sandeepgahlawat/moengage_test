package com.example.sandeep_gahlawat.ui.main.presenter

import android.util.Log
import com.example.sandeep_gahlawat.data.network.Article
import com.example.sandeep_gahlawat.ui.base.presenter.BasePresenter
import com.example.sandeep_gahlawat.ui.main.interactor.MainMVPInteractor
import com.example.sandeep_gahlawat.ui.main.view.MainMVPView
import com.example.sandeep_gahlawat.utils.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class MainPresenter<V : MainMVPView, I : MainMVPInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = disposable
),
    MainMVPPresenter<V, I> {


    override fun getDataFromBlog() {

        getView()?.showProgress()
        interactor?.let { interactor ->
            compositeDisposable.add(
                interactor.getAllArticlesFromDb()
                    .filter {
                        Log.d("checkingDb", "is Empty : ${it.size}")
                        it.isNotEmpty()
                    }
                    .switchIfEmpty(interactor.getDataFromBlog().map { it.article }.doOnNext {
                        interactor.insertIntoDb(it).subscribeOn(Schedulers.io())
                            .subscribe({
                            }, { err ->
                                println(err)
                            })
                    })
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ result ->
                        Log.d("apiresult", result.toString())
//                        performTasks(result)
                        getView()?.let {
                            it.hideProgress()
                            it.displayArticles(result)

                        }

                    }, { err -> println(err) })
            )
        }
    }
}