package com.example.sandeep_gahlawat.ui.main.view

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sandeep_gahlawat.R
import com.example.sandeep_gahlawat.data.network.Article
import com.example.sandeep_gahlawat.data.network.Source
import kotlinx.android.synthetic.main.item_article_list.view.*

class ArticleAdapter(private val articleListItems: MutableList<Article>) : RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        return ArticleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_article_list,parent,false))
    }


    override fun getItemCount() = this.articleListItems.size

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) = holder.let {
        it.clear()
        it.onBind(position)
    }


    internal fun addArticlesToList(articles: List<Article>) {
        Log.d("new articles set",articles.size.toString())
        this.articleListItems.addAll(articles)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun clear() {
//            itemView.coverImageView.setImageDrawable(null)
            itemView.title.text = ""
            itemView.description.text = ""
        }

        fun onBind(position: Int) {

            val ( blogUrl,coverImgUrl,source,title, description,author,date) = articleListItems[position]

            Log.d("checking items",articleListItems[position].title)
            inflateData(blogUrl,coverImgUrl,source,title, description,author,date)
            setItemClickListener(blogUrl)
        }

        private fun setItemClickListener(blogUrl: String?) {
            itemView.setOnClickListener {
                blogUrl?.let {
                    try {
                        val intent = Intent()
                        // using "with" as an example
                        with(intent) {
                            action = Intent.ACTION_VIEW
                            data = Uri.parse(it)
                            addCategory(Intent.CATEGORY_BROWSABLE)
                        }
                        itemView.context.startActivity(intent)
                    } catch (e: Exception) {
                    }
                }

            }
        }

        private fun inflateData(blogUrl: String?, coverImgUrl:String?, source: Source?, title: String?, description: String?,author: String?, date: String?) {
            title?.let { itemView.title.text = it }
            description?.let { itemView.description.text = it }
//            date?.let { itemView.dateTextView.text = it }
//            description?.let { itemView.contentTextView.text = it }
//            coverPageUrl?.let {
//                itemView.coverImageView.loadImage(it)
//            }
        }

    }
}
