package com.example.sandeep_gahlawat.di.builder

import com.example.sandeep_gahlawat.ui.main.MainActivityModule
import com.example.sandeep_gahlawat.ui.main.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    abstract fun bindMainActivity(): MainActivity



}