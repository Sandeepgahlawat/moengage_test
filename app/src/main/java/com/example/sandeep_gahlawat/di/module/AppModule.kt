package com.example.sandeep_gahlawat.di.module

import android.app.Application
import android.content.Context
import com.example.sandeep_gahlawat.data.database.ArticleDbHelper
import com.example.sandeep_gahlawat.data.database.repository.ArticlesRepo
import com.example.sandeep_gahlawat.data.database.repository.ArticlesRepository
import com.example.sandeep_gahlawat.data.network.ApiHelper
import com.example.sandeep_gahlawat.data.network.AppApiHelper
import com.example.sandeep_gahlawat.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideApiHelper(): ApiHelper = AppApiHelper()

    @Provides
    @Singleton
    internal fun provideArticlesDbHelper(context: Context): ArticleDbHelper = ArticleDbHelper(context)

    @Provides
    @Singleton
    internal fun provideArticleRepoHelper(dbHelper: ArticleDbHelper): ArticlesRepo = ArticlesRepository(dbHelper)


    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()



}