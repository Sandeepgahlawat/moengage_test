package com.example.sandeep_gahlawat.data.database.repository

import com.example.sandeep_gahlawat.data.network.Article
import io.reactivex.Observable

interface ArticlesRepo {

    fun isArticlesRepoEmpty(): Observable<Boolean>

    fun insertArticle(questions: List<Article>): Observable<Boolean>

    fun loadArticles(): Observable<List<Article>>
}