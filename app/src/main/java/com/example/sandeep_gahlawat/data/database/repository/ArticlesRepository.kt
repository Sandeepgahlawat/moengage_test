package com.example.sandeep_gahlawat.data.database.repository

import android.content.ContentValues
import android.provider.BaseColumns
import android.util.Log
import com.example.sandeep_gahlawat.data.database.ArticleContract.ArticleEntry
import com.example.sandeep_gahlawat.data.database.ArticleDbHelper
import com.example.sandeep_gahlawat.data.network.Article
import com.example.sandeep_gahlawat.data.network.Source
import io.reactivex.Observable
import javax.inject.Inject

class ArticlesRepository @Inject internal constructor(private val articleDbHelper: ArticleDbHelper) :
    ArticlesRepo {
    override fun isArticlesRepoEmpty(): Observable<Boolean> {
        return Observable.fromCallable {
            val result: Boolean
            val db = articleDbHelper.writableDatabase
            val count = "SELECT count(*) FROM ${ArticleEntry.TABLE_NAME}"
            val mcursor = db.rawQuery(count, null)
            mcursor.moveToFirst()
            val icount = mcursor.getInt(0)
            Log.d("checkingDbCount",icount.toString())
            result = icount <= 0
            result
        }
    }

    override fun insertArticle(articles: List<Article>): Observable<Boolean> {

        return Observable.fromCallable {
            val db = articleDbHelper.writableDatabase
            db.beginTransaction()
            try {
                ContentValues().apply {
                    for (article in articles) {
                        put(ArticleEntry.COLUMN_NAME_TITLE, article.title)
                        put(ArticleEntry.COLUMN_NAME_AUTHOR, article.author)
                        put(ArticleEntry.COLUMN_DESCRIPTION, article.description)
                        put(ArticleEntry.COLUMN_BLOG_URL, article.blogUrl)
                        put(ArticleEntry.COLUMN_IMAGE_URL, article.coverImgUrl)
                        put(ArticleEntry.COLUMN_PUBLISHED_ON, article.date)
                        put(ArticleEntry.COLUMN_SOURCE, article.source?.name)
                        db.insert(ArticleEntry.TABLE_NAME, null, this)
                    }
                }

                db.setTransactionSuccessful()
            } finally {
                db.endTransaction()
            }
            true
        }

    }

    override fun loadArticles(): Observable<List<Article>> {
        return Observable.fromCallable {
            val db = articleDbHelper.readableDatabase

// Define a projection that specifies which columns from the database
// you will actually use after this query.
            val projection = arrayOf(
                BaseColumns._ID,
                ArticleEntry.COLUMN_NAME_TITLE,
                ArticleEntry.COLUMN_NAME_AUTHOR,
                ArticleEntry.COLUMN_DESCRIPTION,
                ArticleEntry.COLUMN_SOURCE,
                ArticleEntry.COLUMN_IMAGE_URL,
                ArticleEntry.COLUMN_BLOG_URL,
                ArticleEntry.COLUMN_PUBLISHED_ON
            )

// Filter results WHERE "title" = 'My Title'
            val selection = ""//"${ArticleEntry.COLUMN_NAME_TITLE} = ?"
            val selectionArgs = ""//arrayOf("My Title")

// How you want the results sorted in the resulting Cursor
            val sortOrder = "${BaseColumns._ID} DESC"

            val cursor = db.query(
                ArticleEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
            )
            val articles = mutableListOf<Article>()
            with(cursor) {
                while (moveToNext()) {
                    val article: Article = Article()
                    article.title = getString(getColumnIndexOrThrow(ArticleEntry.COLUMN_NAME_TITLE))
                    article.author =
                        getString(getColumnIndexOrThrow(ArticleEntry.COLUMN_NAME_AUTHOR))
                    article.blogUrl = getString(getColumnIndexOrThrow(ArticleEntry.COLUMN_BLOG_URL))
                    article.coverImgUrl =
                        getString(getColumnIndexOrThrow(ArticleEntry.COLUMN_IMAGE_URL))
                    article.source =
                        Source("0", getString(getColumnIndexOrThrow(ArticleEntry.COLUMN_SOURCE)))
                    article.description =
                        getString(getColumnIndexOrThrow(ArticleEntry.COLUMN_DESCRIPTION))
                    article.date =
                        getString(getColumnIndexOrThrow(ArticleEntry.COLUMN_PUBLISHED_ON))
                    articles.add(article)
                }
            }
            articles
        }
    }
}