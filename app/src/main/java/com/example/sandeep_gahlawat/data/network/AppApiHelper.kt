package com.example.sandeep_gahlawat.data.network

import com.google.gson.Gson
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observable

class AppApiHelper : ApiHelper {

    override fun getBlogApiCall(): Observable<ArticleResponse> =
        Observable.fromCallable { RequestHandler.sendGet("https://candidate-test-data-moengage.s3.amazonaws.com/Android/news-api-feed/staticResponse.json") }
            .map { value ->  Gson().fromJson(value, ArticleResponse::class.java)
            }

}