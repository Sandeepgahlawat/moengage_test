package com.example.sandeep_gahlawat.data.database

import android.provider.BaseColumns

object ArticleContract {
    // Table contents are grouped together in an anonymous object.
    object ArticleEntry : BaseColumns {
        const val TABLE_NAME = "Articles"
        const val COLUMN_NAME_TITLE = "title"
        const val COLUMN_NAME_AUTHOR = "author"
        const val COLUMN_BLOG_URL = "blogUrl"
        const val COLUMN_DESCRIPTION = "description"
        const val COLUMN_IMAGE_URL = "imageUrl"
        const val COLUMN_SOURCE = "source"
        const val COLUMN_PUBLISHED_ON = "publishedOn"
    }
}