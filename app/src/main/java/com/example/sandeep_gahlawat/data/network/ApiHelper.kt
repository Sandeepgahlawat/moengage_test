package com.example.sandeep_gahlawat.data.network

import io.reactivex.Observable

interface ApiHelper {

    fun getBlogApiCall(): Observable<ArticleResponse>


}