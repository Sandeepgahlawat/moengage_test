package com.example.sandeep_gahlawat.data.network

import com.google.gson.annotations.SerializedName

data class ArticleResponse (

	@SerializedName("status") val status : String,
	@SerializedName("articles") val article : List<Article>
)